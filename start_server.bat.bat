@echo off
:beginning
echo Checking config file...
ping -n 2 127.0.0.1>nul
if not exist dayzsetting.xml goto rename
if not exist serverDZ.cfg goto rename

start "" "DayZServer_x64" -instanceId=1 -config=serverDZ.cfg -profiles=SinglePlayerProject -port=2302 -cpuCount=8 -noFilePatching -dologs -adminlog -freezecheck
exit

:rename
echo.
echo Rename *.example config files...
ping -n 2 127.0.0.1>nul
copy "%CD%\dayzsetting.xml.example" "%CD%\dayzsetting.xml"
ping -n 2 127.0.0.1>nul
copy "%CD%\serverDZ.cfg.example" "%CD%\serverDZ.cfg"
ping -n 2 127.0.0.1>nul
goto beginning